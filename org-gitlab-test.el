;;; org-gitlab-test.el --- Test for org-gitlab.el      -*- lexical-binding: t; -*-

;; Copyright (C) 2019  Toon Claes

;; Author: Toon Claes <toon@iotcl.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; These are a bunch of unit tests for org-gitlab.el.

;;; Code:

(require 'ert)
(require 'org-gitlab)

(ert-deftest org-gitlab-test/compose-uri ()
  "Check if the path is correctly added to the URL."
  (should (equal (org-gitlab--compose-uri "todos")
                 "https://gitlab.com/api/v4/todos/")))

(ert-deftest org-gitlab-test/calculate-deadline ()
  "Check if the deadline is correctly calculated keeping weekends into account."
  (should (equal (org-gitlab--calculate-deadline "2020-07-09 10:00") "2020-07-11 Sat 10:00"))
  (should (equal (org-gitlab--calculate-deadline "2020-07-09 13:00") "2020-07-13 Mon 13:00"))
  (should (equal (org-gitlab--calculate-deadline "2020-07-10 08:00") "2020-07-14 Tue 08:00"))
  (should (equal (org-gitlab--calculate-deadline "2020-07-11 15:00") "2020-07-15 Wed 00:00"))
  (should (equal (org-gitlab--calculate-deadline "2020-07-12 15:00") "2020-07-15 Wed 00:00")))

(provide 'org-gitlab-test)
;;; org-gitlab-test.el ends here
