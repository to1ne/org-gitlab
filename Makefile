# Inspired by https://nullprogram.com/blog/2020/01/22/

.POSIX:
EMACS     = emacs
BATCH     = $(EMACS) --quick --batch $(LOAD_PATH)
LOAD_PATH = --directory $(TOP)
TOP := $(dir $(lastword $(MAKEFILE_LIST)))

compile: org-gitlab.elc org-gitlab-test.elc

test: check
check: org-gitlab-test.elc
	$(BATCH) --load $< --funcall ert-run-tests-batch-and-exit

clean:
	rm -f *.elc

org-gitlab-test.elc: org-gitlab.elc

.SUFFIXES: .el .elc
%.elc: %.el
	$(BATCH) --funcall batch-byte-compile $<

.PHONY: compile test check clean
