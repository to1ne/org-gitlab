;;; org-gitlab.el --- Your GitLab Todos in org-mode -*- lexical-binding: t; -*-

;; Copyright (C) 2017 Toon Claes

;; Author: Toon Claes <toon@iotcl.com>
;; Maintainer: Toon Claes <toon@iotcl.com>
;; Created: 13 Feb 2017
;; Modified: 15 Mar 2021
;; Version: 0.3
;; Package-Version: 20210315.1047
;; Package-Requires: ((emacs "27.0") (org "9"))
;; Keywords: org-mode gitlab todos issues merge-requests
;; URL: https://gitlab.com/to1ne/org-gitlab

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; This `org-gitlab' minor synchronizes your GitLab Todos, issues, and
;; merge requests to a local org-mode file.  It runs in the background
;; and refreshes existing headlines and properties.  The
;; synchronization is one-way and only syncs from GitLab to a local
;; file.
;;
;; To synchronize a heading with GitLab, you should specify some
;; properties.  The heading should look something like:
;;   * Synchronize all open merge requests assigned to me
;;     :PROPERTIES:
;;     :org-gitlab: merge-requests
;;     :assignee:   123456
;;     :state:      opened
;;     :END:
;;
;; `org-gitlab' use the following properties:
;; TODO (put in README)

;;
;; Please see README.org from the same repository for documentation.

;;; Code:

(require 'json)
(require 'url-http)
(require 'org)

(defgroup org-gitlab nil
  "GitLab to org-mode synchronization settings"
  :group 'org)

(defcustom org-gitlab-token nil
  "Private GitLab token used to authenticate yourself with GitLab."
  :group 'org-gitlab
  :type '(string (const nil)))

(defcustom org-gitlab-server "https://gitlab.com"
  "GitLab server to communicate with.

By default gitlab.com is used."
  :group 'org-gitlab
  :type 'string)

(defcustom org-gitlab-mode-lighter "org-gl "
  "Lighter shown in the modeline when `org-gitlab-mode' is active."
  :group 'org-gitlab
  :type 'string)

(defcustom org-gitlab-mode-keymap-prefix (kbd "C-c g")
  "Org-gitlab keymap prefix."
  :group 'org-gitlab
  :type 'string)

(defcustom org-gitlab-user-id nil
  "The GitLab user id for the current user.

When it is not specified, it is automatically loaded from GitLab and
saved in customize."
  :group 'org-gitlab
  :type 'integer)

(defcustom org-gitlab-archive-todo-function nil
  "Can be used to automatically archive Todos that are no longer active.

When Todos are synced from GitLab, it removes the TODO state from
all items that are not returned from the API. You can set this to
a function to archive these items, for example with
`org-archive-subtree'."
  :group 'org-gitlab
  :type 'function)

(defun org-gitlab--token ()
  "Return the token from customize or from environment."
  (or org-gitlab-token
      (auth-source-pick-first-password
       :require '(:secret)
       :host org-gitlab-server
       :user "org-gitlab")
      (getenv "GITLAB_TOKEN")
      (error "GitLab token not set")))

(defun org-gitlab--get-headers ()
  "Return the HTTP headers for Gitlab API."
  ;;(if (not (s-blank? gitlab-token-id))
  (list (cons "PRIVATE-TOKEN" (org-gitlab--token))
        (cons "connection" "close")))

(defun org-gitlab--compose-uri (path)
  "Compose the full URL for the endpoint at PATH."
  (mapconcat (lambda (x)
               (file-name-as-directory (subst-char-in-string ?- ?_ x)))
             `(,org-gitlab-server "api/v4" ,path) nil))

(defun org-gitlab--get-request (path &optional params)
  "Send a request to the GitLab server.

  PATH is the path of the endpoint and PARAMS are the additional parameters."
  (let* ((params (append (or params '()) (list (list "per_page" "100"))))
         (url-request-extra-headers (org-gitlab--get-headers))
         (url-request-method "GET")
         (url-request-noninteractive t)
         (url-show-status nil)
         (url (org-gitlab--compose-uri path))
         (args (url-build-query-string params))
         (json-array-type 'list))
    (with-current-buffer (url-retrieve-synchronously (concat url "?" args) t)
      (unless (= 200 url-http-response-status)
        (error "Failed to GET %s error: %d" url url-http-response-status))
      (goto-char url-http-end-of-headers)
      (json-read))))

(defun org-gitlab--get-issuables (class params)
  "Fetch all issuables of type CLASS from api using PARAMS."
  (org-gitlab--get-request class params))

(defun org-gitlab--get-todos (params)
  "Fetch all Todos from api using PARAMS."
  (reverse (org-gitlab--get-request "todos" params)))

(defun org-gitlab--params-at-point ()
  "Get the query parameters for the heading at point."
  (let ((author (org-entry-get (point) "author"))
        (assignee (org-entry-get (point) "assignee"))
        (reviewer (org-entry-get (point) "reviewer"))
        (scope (org-entry-get (point) "scope"))
        (state (org-entry-get (point) "state"))
        (params '()))
    (if author
        (setq params (append params (list (list "author_id" author)))))
    (if assignee
        (setq params (append params (list (list "assignee_id" assignee)))))
    (if reviewer
        (setq params (append params (list (list "reviewer_username" reviewer)))))
    (if state
        (setq params (append params (list (list "state" state)))))
    (if scope
        (setq params (append params (list (list "scope" scope)))))
    (if (seq-empty-p params)
        (error "No query parameters found"))
    params))

(defun org-gitlab--singularize-class (class)
  "Return the singular form of CLASS."
  (cond ((equal "issues" class) "issue")
        ((equal "merge-requests" class) "merge-request")))

(defun org-gitlab--try-set-property (property issuable &rest keys)
  "Set the PROPERTY to the current heading.

  Read from ISSUABLE and use a list of KEYS to fetch the
  value.  If no KEYS are given, use PROPERTY instead."
  (unless keys (setq keys (list (make-symbol property))))
  (let* ((val issuable))
    (dolist (k (or keys (list (make-symbol property))))
      (setq val (alist-get k val)))
    (if val (org-set-property property val))))

(defun org-gitlab--format-ref (issuable &optional key)
  "Format the ISSUABLE to a reference using KEY or `web_url'."
  (let* ((web-url (alist-get (or key 'web_url) issuable))
         (web-filename (substring (url-filename (url-generic-parse-url web-url)) 1))
         (ref (replace-regexp-in-string "\\(/-\\)?/merge_requests/" "!"
               (replace-regexp-in-string "\\(/-\\)?/issues/" "#" web-filename))))
    (org-link-make-string web-url ref)))

(defun org-gitlab--format-user (user)
  "Format a USER to a clickable username."
  (let ((web-url (alist-get 'web_url user))
        (username (concat "@" (alist-get 'username user))))
    (org-link-make-string web-url username)))

(defun org-gitlab--insert-subheading (&optional pos)
  "Insert a subheading below the one at POS or point."
  (if pos (goto-char pos))
  (org-insert-heading-respect-content)
  (org-do-demote)
  (beginning-of-line) (newline)) ;; add some spacing after previous heading

(defun org-gitlab--sync-issuables (class &optional params)
  "Synchronize issuables from GitLab server to sub headings.

Specify the CLASS of the issuables and optionally specify PARAMS
to filter."
  (save-excursion
    (unless params
      (setq params (org-gitlab--params-at-point)))
    (let ((issuables (org-gitlab--get-issuables class params))
          (heading-pos (point))
          (synced-at (org-gitlab--format-time (current-time)))
          pos)
      (dolist (issuable issuables)
        (let ((attr (number-to-string (alist-get 'id issuable))))
          (if (setq pos (org-find-property "id" attr))
              (goto-char pos)
            (goto-char heading-pos)
            (org-gitlab--insert-subheading heading-pos)
            (org-set-property "org-gitlab" (org-gitlab--singularize-class class))
            (org-set-property "id" attr))
          (org-edit-headline (alist-get 'title issuable))
          (org-todo "TODO")
          (org-set-property "synced-at" synced-at)
          (org-gitlab--try-set-property "branch" issuable 'source_branch)
          (org-set-property "ref" (org-gitlab--format-ref issuable))
          (org-gitlab--try-set-property "author" issuable 'author 'username)
          (org-gitlab--try-set-property "assignee" issuable 'assignee 'username)))
    synced-at)))

(defun org-gitlab--titlize-todo (todo)
  "Generate title for TODO."
  (let* ((action (alist-get 'action_name todo))
         (ref (org-gitlab--format-ref todo 'target_url))
         (author (alist-get 'author todo)))
    (if author
        (concat (org-gitlab--format-user author)
                (cond ((equal "assigned" action) " assigned ")
                      ((equal "mentioned" action) " mentioned you on ")
                      ((equal "marked" action) " added a todo for ")
                      ((equal "approval_required" action) " asked your approval for ")
                      ((equal "directly_addressed" action) " directly addressed you on ")
                      ((equal "build_failed" action) "'s build failed for ")
                      (t (concat " " action " ")))
                ref)
      (concat (cond ((equal "build_failed" action) "The build failed for ")
                    ((equal "unmergeable" action) "Merge is not possible on ")
                    (t (concat action " on ")))
              ref))))

(defun org-gitlab--format-time (time)
  "Format the TIME into an proper 'org-mode' format."
  (format-time-string (org-time-stamp-format t t)
                      (if (stringp time) (date-to-time time) time)))

(defun org-gitlab--calculate-deadline (time-string)
  "Calculate the deadline, two business days after TIME-STRING."
  (let* ((decoded-time (decode-time (date-to-time time-string)))
         (dow (decoded-time-weekday decoded-time))
         (h (decoded-time-hour decoded-time))
         (delta
          (cond
           ((and (= 4 dow) (< 12 h)) ;; After: Thu 12:00
            96)
           ((= 5 dow)                ;;    On: Fri
            96)
           ((= 6 dow)                ;;    On: Sat
            (- 96 h))
           ((= 0 dow)                ;;    On: Sun
            (- 72 h))
           (t                        ;; Any other day
            48))))
    (format-time-string (substring (org-time-stamp-format t t) 1 -1)
                        (apply #'encode-time
                               (decoded-time-add decoded-time
                                                 (make-decoded-time :hour delta))))))

(defun org-gitlab--sync-todos (&optional params)
  "Synchronize your Todos from GitLab server to sub headings.
Optionally specify PARAMS to filter."
  (save-excursion
    (unless params
      (setq params (org-gitlab--params-at-point)))
    (let ((todos (org-gitlab--get-todos params))
          (heading-pos (point))
          (synced-at (org-gitlab--format-time (current-time)))
          pos)
      (dolist (todo todos)
        (let ((attr (number-to-string (alist-get 'id todo)))
              (created-at (alist-get 'created_at todo)))
          (if (setq pos (org-find-property "id" attr))
              (goto-char pos)
            ;; TODO check out https://www.reddit.com/r/emacs/comments/c31lth/org_api_text_of_section/
            (org-gitlab--insert-subheading heading-pos)
            (org-edit-headline (org-gitlab--titlize-todo todo))
            (org-set-property "org-gitlab" "todo")
            (org-set-property "id" attr)
            (org-set-property "title" (format "*%s*" (alist-get 'title (alist-get 'target todo))))
            (org-set-property "action" (alist-get 'action_name todo))
            (org-set-property "created-at"
                              (org-gitlab--format-time created-at))
            (if (equal "MergeRequest" (alist-get 'target_type todo))
                (org-add-planning-info 'deadline (org-gitlab--calculate-deadline created-at)))
            (goto-char (cdr (org-get-property-block)))
            (forward-line)
            (let ((body-start (point)))
              (insert "\n#+BEGIN_QUOTE\n")
              (insert (alist-get 'body todo))
              (insert "\n#+END_QUOTE\n")
              (indent-rigidly body-start (point) 3)
              (save-mark-and-excursion
                (forward-line -1)
                (set-mark (point))
                (goto-char body-start)
                (forward-line 1)
                (org-fill-paragraph nil t)))
            (newline))
          (org-todo "TODO")
          (org-set-property "synced-at" synced-at)))
      synced-at)))

(defun org-gitlab--untodo-subtree ()
  "Remove TODO from all headings in tree."
  (org-map-entries '(org-todo "") "/+TODO" 'tree))

;;;###autoload
(defun org-gitlab-sync-at-point ()
  "Synchronize the heading at point.

It has to contain the `org-gitlab' property.  This property can have
the value: `issues', `merge-requests', or `todos'.
For the values `issues' and `merge requests', the heading must also
contain one of the following properties: `author' or `assignee'."
  (interactive)
  (save-excursion
    (save-restriction
      (org-narrow-to-subtree)
      (let ((params (org-gitlab--params-at-point))
            (class (org-entry-get (point) "org-gitlab")))
        (cond ((or (equal "issues" class)
                   (equal "merge-requests" class))
               (let ((synced-at (org-gitlab--sync-issuables class params)))
                 (org-map-entries (lambda ()
                                    (unless (string-equal synced-at (org-entry-get (point) "synced-at" ()))
                                      (org-todo "DONE")))
                                  "/+TODO" 'tree)))
              ((equal "todos" class)
               (let ((synced-at (org-gitlab--sync-todos params)))
                 (org-map-entries (lambda ()
                                    (unless (string-equal synced-at (org-entry-get (point) "synced-at" ()))
                                      ;(when org-gitlab-archive-todo-function ... )
                                      (org-todo "DONE")))
                                  "/+TODO" 'tree)))
                 )))))

;;;###autoload
(defun org-gitlab-sync-all ()
  "Synchronize everything in the current file with the GitLab server."
  (interactive)
  (save-excursion
    (let (org-gitlab-heading)
      ;; TODO this isn't very efficient, since it shouldn't sync
      ;; children of headings it synced already
      (while (setq org-gitlab-heading (org-find-property "org-gitlab"))
        (goto-char org-gitlab-heading)
        (org-gitlab-sync-at-point)
        ;; narrow buffer to sync the next org-gitlab heading
        (widen)
        (narrow-to-region org-gitlab-heading (point-max)))) ; TODO jump to next
    (widen)))

;;;###autoload
(defun org-gitlab-insert-properties ()
  "Insert org-gitlab properties to the current heading.

To make the current heading synchronize with your GitLab instance,
this function can be used to set the properties used by org-gitlab to
run the query for sub-headings.

It inserts:
 :org-gitlab: To specify what to sync.  It should be one of:
   - issues
   - merge-requests
   - todos
 :author: To limit everything authored by the user specified.
 :assignee: To limit everything assigned to the user specified.
 :state: To limit to the specified state."
  (interactive)
  ;; TODO implement me
    )

;;; Minor mode
(defvar org-gitlab-command-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "a") #'org-gitlab-sync-all)
    (define-key map (kbd "h") #'org-gitlab-sync-at-point)
    (define-key map (kbd "i") #'org-gitlab-insert-properties)
    map)
  "Keymap for Org-gitlab commands after `org-gitlab-mode-keymap-prefix'.")
(fset 'org-gitlab-command-map org-gitlab-command-map)

(defvar org-gitlab-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map org-gitlab-mode-keymap-prefix 'org-gitlab-command-map)
    map)
  "Keymap for Org-gitlab mode.")

(defun org-gitlab--clock-in ()
  "A hook called when clocking in."
  (org-tree-to-indirect-buffer)
  (with-selected-frame org-indirect-dedicated-frame
  ;; (switch-to-buffer-other-window (window-buffer
  ;;                                 (frame-root-window org-indirect-dedicated-frame)))
    (text-scale-adjust +4)
    (org-show-all)))

(defun org-gitlab-mode--on ()
  "Turn Org-gitlab mode on and enable all the hooks."
  (add-hook 'org-clock-in-hook #'org-gitlab--clock-in nil t))

(defun org-gitlab-mode--off ()
  "Turn Org-gitlab mode off and clean up the hooks."
  (remove-hook 'org-clock-in-hook #'org-gitlab--clock-in))

;;;###autoload
(define-minor-mode org-gitlab-mode
  "Minor mode to synchronize org file with GitLab.

When called interactively, toggle `org-gitlab-mode'.  With prefix
ARG, enable `org-gitlab-mode' if ARG is positive, otherwise disable
it.
\\{org-gitlab-mode-map}"
  :init-value nil
  :lighter org-gitlab-mode-lighter
  :group 'org-gitlab
  :keymap org-gitlab-mode-map
  (if org-gitlab-mode
      (org-gitlab-mode--on)
    (org-gitlab-mode--off)))

(provide 'org-gitlab)

;;; org-gitlab.el ends here
